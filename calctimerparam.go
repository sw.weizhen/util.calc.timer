package swcalctimer

import (
	"fmt"
	"time"
)

func (ref *CALCTimer) stdUTC() {
	t, err := time.Parse(ref.inFMT, ref.in)
	if err != nil {
		ref.ok = false
		ref.err = err
	}

	ref.uxt = t.Unix()
	ref.uxtcur = t.Unix()
	ref.ltCur = t
	ref.ltOrg = t
}

func (ref *CALCTimer) String() string {
	if ref.ok {
		return ref.ltCur.Format(CST_FMT_YMDHMS)
	}

	return ref.err.Error()
}

func (ref *CALCTimer) ChkTime() bool {
	return ref.ok
}

// func (ref *CALCTimer) GetUnixTimeOrg() int64 {
// 	return ref.uxt
// }

// func (ref *CALCTimer) GetLibTimeOrg() time.Time {
// 	return ref.ltOrg
// }

func (ref *CALCTimer) GetUnixTimeCur() int64 {
	return ref.uxtcur
}

func (ref *CALCTimer) GetLibTimeCur() time.Time {
	return ref.ltCur
}

func (ref *CALCTimer) GetInFMT() string {
	return ref.inFMT
}

func (ref *CALCTimer) GetYear() string {
	return fmt.Sprintf("%04d", ref.ltCur.Year())
}

func (ref *CALCTimer) GetMonth() string {
	return fmt.Sprintf("%02d", int(ref.ltCur.Month()))
}

func (ref *CALCTimer) GetDay() string {
	return fmt.Sprintf("%02d", ref.ltCur.Day())
}

func (ref *CALCTimer) GetHour() string {
	return fmt.Sprintf("%02d", ref.ltCur.Hour())
}

func (ref *CALCTimer) GetMinute() string {
	return fmt.Sprintf("%02d", ref.ltCur.Minute())
}

func (ref *CALCTimer) GetSecond() string {
	return fmt.Sprintf("%02d", ref.ltCur.Second())
}
