package swcalctimer

import (
	"time"
)

func New(opts ...optT) *CALCTimer {
	now := time.Now()
	name, offset := now.Zone()

	obj := &CALCTimer{
		in:     now.Format(CST_FMT_YMDHMS),
		inFMT:  CST_FMT_YMDHMS,
		uxt:    now.Unix(),
		uxtcur: now.Unix(),
		ok:     true,
		ltOrg:  now,
		ltCur:  now,
		zone:   name,
		offset: offset,
		prev:   nil,
		next:   nil,
	}

	for _, opt := range opts {
		opt(obj)
	}

	obj.stdUTC()

	return obj
}
