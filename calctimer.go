package swcalctimer

import (
	"time"
)

type CALCTimer struct {
	in     string
	inFMT  string
	uxt    int64
	uxtcur int64
	err    error
	ok     bool

	ltOrg  time.Time
	ltCur  time.Time
	zone   string
	offset int

	prev *CALCTimer
	next *CALCTimer
}
