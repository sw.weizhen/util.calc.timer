package swcalctimer

type optT func(*CALCTimer)

func OptFMTinput(inFMT string) optT {
	return func(ref *CALCTimer) {
		ref.inFMT = inFMT
	}
}

func OptInput(in string) optT {
	return func(ref *CALCTimer) {
		ref.in = in
	}
}

func OptTimeZone(zone string) optT {
	return func(ref *CALCTimer) {
		ref.zone = zone
	}
}
