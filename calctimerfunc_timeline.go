package swcalctimer

import (
	"errors"
	"fmt"
)

func calcINVL(calcT1, calcT2 *CALCTimer, opt *TimelineOpt, baseLayout string, addSec int) ([]string, error) {
	l1 := calcT1.Layout(baseLayout)
	l2 := calcT2.Layout(baseLayout)

	cmp := calcT1.Compare(calcT2)
	if cmp == 1 {
		return make([]string, 0), fmt.Errorf("%s error: headT is greater than tailT, t1: %s, t2: %s", opt.INVLMode, l1, l2)
	}

	if cmp == 0 {
		if opt.Overlap {
			return []string{calcT1.Layout(opt.OutFMT)}, nil
		}

		return make([]string, 0), nil
	}

	tlls := []string{calcT1.Layout(opt.OutFMT)}
	for calcT1.Compare(calcT2) < 0 {
		calcT1.Add(addSec)
		calcT1.Layout(baseLayout)

		tlls = append(tlls, calcT1.Layout(opt.OutFMT))
	}

	if !opt.Overlap {
		tlls = tlls[:len(tlls)-1]
	}

	return tlls, nil
}

func (ref *CALCTimer) CALCTimelineOnHour(overlap bool, tailT ...*CALCTimer) ([]map[string]string, error) {
	opt := []optTL{
		OptINVLMode(ENUM_INVL_HOUR),
		OptOutFMT(CST_FMT_YMDH),
		OptLimitTimeOverlap(overlap),
	}

	if len(tailT) != 0 {
		opt = append(opt, OptLimitTime(tailT[0]))
	}

	tlls, err := ref.CALCTimeline(opt...)

	if err != nil {
		return make([]map[string]string, 0), err
	}

	size := len(tlls)
	tllsOH := make([]map[string]string, size)
	for i := 0; i < size; i++ {
		mpHT := map[string]string{
			cst_KEY_ONHOUR_H: tlls[i] + cst_TEMP_SUFFIX_00,
			cst_KEY_ONHOUR_T: tlls[i] + cst_TEMP_SUFFIX_59,
		}

		tllsOH[i] = mpHT
	}

	return tllsOH, nil
}

func (ref *CALCTimer) CALCTimeline(opts ...optTL) ([]string, error) {
	optParam := getINVLOpt(opts...)

	if !ref.ok {
		return make([]string, 0), fmt.Errorf("CALCTimeline parameter error, headT: %v", ref)
	}

	if !optParam.LimitTime.ok {
		return make([]string, 0), fmt.Errorf("CALCTimeline parameter error, tailT: %v", optParam.LimitTime)
	}

	switch optParam.INVLMode {
	case ENUM_INVL_HOUR:
		return calcINVL(ref, optParam.LimitTime, optParam, CST_FMT_YMDH, cst_UNIT_HOUR)

	case ENUM_INVL_DAY:
		return calcINVL(ref, optParam.LimitTime, optParam, CST_FMT_YMD, cst_UNIT_DAY)

	case ENUM_INVL_MONTH:
		return calcINVL(ref, optParam.LimitTime, optParam, CST_FMT_YM, cst_UNIT_MONTH_31)

	case ENUM_INVL_YEAR:
		return calcINVL(ref, optParam.LimitTime, optParam, CST_FMT_Y, cst_UNIT_YEAR_366)

	default:
		return make([]string, 0), errors.New("CALCTimeline error: unknown INVL mode")
	}
}
