package swcalctimer

import (
	"time"
)

func (ref *CALCTimer) Layout(tmFMT string) string {
	nv := ref.ltCur.Format(tmFMT)
	t, err := time.Parse(tmFMT, nv)

	ref.ltCur = t
	ref.uxtcur = t.Unix()
	if err != nil {
		ref.ok = false
		ref.err = err
		ref.ltCur = ref.ltOrg
		ref.uxtcur = ref.uxt
	}

	return nv
}

func (ref *CALCTimer) Add(sec int) *CALCTimer {
	ref.ltCur = ref.ltCur.Add(time.Second * time.Duration(sec))
	ref.uxtcur = ref.ltCur.Unix()

	return ref
}

func (ref *CALCTimer) Compare(ctm *CALCTimer) int {
	if !ref.ok {
		return -9
	}

	if !ctm.ok {
		return -10
	}

	if ref.uxtcur == ctm.uxtcur {
		return 0
	}

	if ref.uxtcur > ctm.uxtcur {
		return 1
	}

	return -1
}
