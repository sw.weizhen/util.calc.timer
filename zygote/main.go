package main

import (
	"fmt"

	ct "gitlab.com/sw.weizhen/util.calc.timer"
)

func main() {

	t1 := ct.New(ct.OptInput("2022-02-06 04:13:40"))

	// tlls, err := t1.CALCTimeline(
	// 	ct.OptINVLMode(ct.ENUM_INVL_HOUR),
	// 	ct.OptLimitTime(ct.New(ct.OptInput("2022-03-01 05:13:40"))),
	// 	ct.OptOverlap(true),
	// 	ct.OptOutFMT(ct.CST_FMT_YMDH),
	// )

	tlls, err := t1.CALCTimelineOnHour(
		false,
		// ct.New(ct.OptInput("2022-03-01 05:13:40")),
	)

	if err != nil {
		fmt.Printf("tlls error: %v\n", err)
	}

	for _, v := range tlls {
		fmt.Println(v)
	}

	fmt.Println("**************************************")
	t2l, err := t1.CALCTimeline(
		ct.OptINVLMode(ct.ENUM_INVL_HOUR),
		// ct.OptLimitTime(ct.New(ct.OptInput("2022-03-01 05:13:40"))),
		ct.OptLimitTimeOverlap(true),
		ct.OptOutFMT(ct.CST_FMT_YMDH),
	)

	if err != nil {
		fmt.Printf("t2l error: %v\n", err)
	}

	for _, v := range t2l {
		fmt.Println(v)
	}
}
