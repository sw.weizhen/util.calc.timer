package swcalctimer

type enumINVLMode int

const (
	cstINVL_Y  = "INVL_YEAR"
	cstINVL_M  = "INVL_MONTH"
	cstINVL_D  = "INVL_DAY"
	cstINVL_H  = "INVL_HOUR"
	cstINVL_OH = "INVL_ON_HOUR"
)

const (
	ENUM_INVL_YEAR enumINVLMode = iota
	ENUM_INVL_MONTH
	ENUM_INVL_DAY
	ENUM_INVL_HOUR
	ENUM_INVL_ON_HOUR
)

func (ref enumINVLMode) String() string {
	return [...]string{
		cstINVL_Y,
		cstINVL_M,
		cstINVL_D,
		cstINVL_H,
		cstINVL_OH,
	}[ref]
}
