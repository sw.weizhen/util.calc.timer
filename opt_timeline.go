package swcalctimer

type TimelineOpt struct {
	OutFMT   string
	Overlap  bool
	INVLMode enumINVLMode

	LimitTime *CALCTimer
}

type optTL func(*TimelineOpt)

func OptOutFMT(outFMT string) optTL {
	return func(ref *TimelineOpt) {
		ref.OutFMT = outFMT
	}
}

func OptLimitTimeOverlap(overlap bool) optTL {
	return func(ref *TimelineOpt) {
		ref.Overlap = overlap
	}
}

func OptINVLMode(invlMode enumINVLMode) optTL {
	return func(ref *TimelineOpt) {
		ref.INVLMode = invlMode
	}
}

func OptLimitTime(limitTime *CALCTimer) optTL {
	return func(ref *TimelineOpt) {
		ref.LimitTime = limitTime
	}
}

func getINVLOpt(opts ...optTL) *TimelineOpt {
	obj := &TimelineOpt{
		OutFMT:    CST_FMT_YMDH,
		Overlap:   true,
		INVLMode:  ENUM_INVL_HOUR,
		LimitTime: New(),
	}

	for _, opt := range opts {
		opt(obj)
	}

	return obj
}
