package swcalctimer

const (
	CST_FMT_YMDHMS     = "2006-01-02 15:04:05"
	CST_FMT_YMDHM      = "2006-01-02 15:04"
	CST_FMT_YMDH       = "2006-01-02 15"
	CST_FMT_YMD        = "2006-01-02"
	CST_FMT_YM         = "2006-01"
	CST_FMT_Y          = "2006"
	CST_FMT_YMDHMS_CMP = "20060102150405"
	CST_FMT_YMDHM_CMP  = "200601021504"
	CST_FMT_YMDH_CMP   = "2006010215"
	CST_FMT_YMD_CMP    = "20060102"
	CST_FMT_YM_CMP     = "200601"
)

const (
	cst_UNIT_MINUTE       = 60
	cst_UNIT_HOUR         = 60 * 60
	cst_UNIT_DAY          = 60 * 60 * 24
	cst_UNIT_WEEK         = 60 * 60 * 24 * 7
	cst_UNIT_MONTH_JAN_28 = 60 * 60 * 24 * 28
	cst_UNIT_MONTH_JAN_29 = 60 * 60 * 24 * 29
	cst_UNIT_MONTH_30     = 60 * 60 * 24 * 30
	cst_UNIT_MONTH_31     = 60 * 60 * 24 * 31
	cst_UNIT_YEAR_365     = 60 * 60 * 24 * 365
	cst_UNIT_YEAR_366     = cst_UNIT_YEAR_365 + cst_UNIT_DAY
)

const (
	cst_TEMP_SUFFIX_00 = ":00:00"
	cst_TEMP_SUFFIX_59 = ":59:59"
)

const (
	cst_KEY_ONHOUR_H = "h"
	cst_KEY_ONHOUR_T = "t"
)
